class FieldPart {
    constructor(data) {
        // 回答値の配列
        this.data = data

        // データの長さ
        const length = data.length;
        this.length = length;

        // 期待値（['1', '2', '3', '4', '5', '6', '7', '8', '9']）を生成する。
        this.expected = [...Array(length).keys()].map(i => (i + 1).toString());
    }

    isComplete() {
        const values = this.data;
        const expectedArray = this.expected;

        // 入力配列を並び替える。
        const sortedArray = values.slice().sort((a, b) => a - b);

        // 並び替えた結果が期待値と一致することを確認
        const isCorrect = sortedArray.length === expectedArray.length && sortedArray.every((value, index) => value == expectedArray[index]);

        if (isCorrect) {
            return true;
        } else {
            return false;
        }
    }
}

class FieldData {
    constructor(data) {
        // 一辺の長さ
        const sideLength = 9;
        this.sideLength = sideLength;

        // 回答値の配列
        this.data = [
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", "", ""]
        ];
        if ( data !== undefined ) {
            if (typeof data.forEach !== 'function') {
                throw new TypeError(`Unsupported type of data. data: ${data}`);
            }
            if (data.length !== sideLength) {
                throw new TypeError(`Length of 'data' must be ${sideLength}. data: ${data}`);
            }
            data.forEach((array, row) => {
                if (typeof array.forEach !== 'function') {
                    throw new TypeError(`Unsupported type of data. data: ${data}`);
                }
                if (array.length !== sideLength) {
                    throw new TypeError(`Length of an array in 'data' must be ${sideLength}. data: ${data}`);
                }
                array.forEach((value, column) => {
                    const hasUpdated = this.set(row, column, value);
                    if (!hasUpdated) {
                        throw TypeError(`Invalid value in data. data: ${data}`);
                    }
                });
            });
        }
    }

    get(row, column) {
        return this.data[row][column];
    }

     /**
     * Sets a value to data. The value must be '1'-'9' or emtpy('').
     * Returns true when data is updated and false when not.
     * @param {number} row
     * @param {number} column
     * @param {string} value
     * @return {boolean}
     */ 
     set(row, column, value) {
        const allowedLetters = ['', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        if (allowedLetters.indexOf(value) === -1) {
            return false
        }
        this.data[row][column] = value;
        return true
}

    getRow(id) {
        const values = this.data[id];
        return new FieldPart(values);
    }

    getColumn(id) {
        const sideLength = this.sideLength;
        const values = [...Array(sideLength).keys()].map(i => this.data[i][id]);
        return new FieldPart(values);
    }

    getSquare(id) {
        const sideLength = this.sideLength;
        const squareSideLength = Math.sqrt(sideLength);

        const right = (id % squareSideLength) * squareSideLength;
        const down = Math.floor(id / squareSideLength) * squareSideLength;

        const values = [
            this.data[down][right],
            this.data[down][right + 1],
            this.data[down][right + 2],
            this.data[down + 1][right],
            this.data[down + 1][right + 1],
            this.data[down + 1][right + 2],
            this.data[down + 2][right],
            this.data[down + 2][right + 1],
            this.data[down + 2][right + 2],
        ];
        return new FieldPart(values);
    }

    isComplete() {
        const sideLength = this.sideLength;
        for (let i = 0; i < sideLength; i++) {
            if (!this.getRow(i).isComplete()) {
                return false;
            }
            if (!this.getColumn(i).isComplete()) {
                return false;
            }
            if (!this.getSquare(i).isComplete()) {
                return false;
            }
        }
        return true;
    }
}

/**
 * Log of a change on FieldData.
 */
class DataLogRecord {
    /**
     * @param {number} row Row number of FieldData changed.
     * @param {number} column Column number of FieldData changed.
     * @param {string} value The value after the change.
     * @param {boolean} isInitial True when change is made to create the problem, false when to solve the problem.
     */
    constructor(row, column, value, isInitial) {
        this.timestamp = Date.now();
        this.row = row;
        this.column = column;
        this.value = value;
        this.isInitial = isInitial;
    }
}

/**
 * Manages change log of FieldData.
 */
class DataLogs {
    constructor(logs) {
        this.logs = []
        if (logs !== undefined) {
            if (typeof logs.forEach !== 'function') {
                throw new TypeError(`Unsupported type of data. logs: ${logs}`);
            }
            logs.forEach(log => {
                this.add(log.row, log.column, log.value, log.isInitial);
            });
        }
    }

    /**
     * Creates log record and add to list.
     * @param {number} row Row number of FieldData changed.
     * @param {number} column Column number of FieldData changed.
     * @param {string} value The value after the change.
     * @param {boolean} isInitial True when change is made to create the problem, false when to solve the problem.
     */
    add(row, column, value, isInitial) {
        const logs = this.logs;
        const record = new DataLogRecord(row, column, value, isInitial);
        logs.push(record);
    }

    forEach(callbackFn) {
        this.logs.forEach(callbackFn)
    }
}

/**
 * Presentation for DataLogs.
 */
class LogField {
    constructor(table, logs) {
        this.table = table;
        this.logs = logs;
    }

    update() {
        const table = this.table;
        const logs = this.logs;

        table.innerHTML = '';
        const headerRow = table.insertRow();
        ['seq', 'timestamp', 'row', 'column', 'value', 'isInitial'].forEach(value => {
            const headerCell = headerRow.insertCell();
            headerCell.innerHTML = value;
        });

        logs.forEach((log, index) => {
            const dataRow = table.insertRow(1);
            const seq = index + 1;
            ['seq', 'timestamp', 'row', 'column', 'value', 'isInitial'].forEach(value => {
                const dataCell = dataRow.insertCell();
                if (value == 'seq') {
                    dataCell.innerHTML = seq;
                    return;
                }
                dataCell.innerHTML = log[value];
            });
        });
    }
}

class DataField {
    constructor(table, logTable, data) {
        const logs = new DataLogs();
        const logField = new LogField(logTable, logs);

        this.table = table;
        this.data = data;
        this.logs = logs;
        this.hasStarted = false;
        this.tableDataMap = {};
        this.logField = logField;
    }

    initialize() {
        // tableの内容を削除してから再作成する。
        // table内のinputは値を変更される度にonUpdateを呼び出す。
        this.hasStarted = false;
        const table = this.table;
        const data = this.data;
        const tableDataMap = this.tableDataMap;
        const logField = this.logField;

        const sideLength = data.sideLength;
        table.innerHTML = '';
        for (let i = 0; i < sideLength; i++) {
            const row = table.insertRow();
            for (let j = 0; j < sideLength; j++) {
                const inputId = `${i}-${j}`

                const cell = row.insertCell();
                const input = document.createElement('input');
                input.id = inputId;
                input.type = 'text';
                input.maxLength = 1;
                input.addEventListener('change', () => {
                    this.onUpdate(input);
                });
                cell.appendChild(input);

                tableDataMap[inputId] = [i, j];
            }
        }

        // table内のinputの内容を初期値に変更する。
        table.querySelectorAll('input').forEach(input => {
            const inputId = input.id;
            const position = tableDataMap[inputId];
            const row = position[0];
            const column = position[1];
            const value = data.get(row, column);

            if (value === '') {
                // 値が空白の場合は何もする必要なし。
                return;
            }

            input.value = value;
            input.dispatchEvent(new Event('change'));
        })
        
        logField.update();
    }

    start() {
        this.hasStarted = true;
    }

    finish() {
        const data = this.data;
        if (data.isComplete()) {
            alert("Correct!");
        } else {
            alert("Not yet!");
        }
    }

    onUpdate(input) {
        // table内のinputの値を変更されたら呼び出される。
        // FieldDataを更新する。
        // DataLogsを更新する。
        const data = this.data;
        const tableDataMap = this.tableDataMap;
        const logs = this.logs;
        const hasStarted = this.hasStarted;
        const logField = this.logField;

        const inputId = input.id;
        const position = tableDataMap[inputId];
        const row = position[0];
        const column = position[1];
        const value = input.value;
        // console.log('ID:', inputId, 'row:', row, 'column:', column, 'value:', value);

        const hasChanged = data.set(row, column, value);
        if (!hasChanged) {
            input.value = data.get(row, column);
        }
        if (hasChanged) {
            logs.add(row, column, value, !hasStarted, '');
            console.log(logs);
            logField.update();
        }
        console.log(data);
    }
}


class GameController {
    constructor(table, logTable) {
        this.table = table;
        this.logTable = logTable;
        this.dataField;
    }

    initialize() {
        // ゲーム画面を生成する。
        // プレイヤーが問題を入力する。
        const table = this.table;
        const logTable = this.logTable;

        // const data = new FieldData();
        const data = new FieldData([
            ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
            ['4', '5', '6', '7', '8', '9', '1', '2', '3'],
            ['7', '8', '9', '1', '2', '3', '4', '5', '6'],
            ['2', '3', '4', '5', '6', '7', '8', '9', '1'],
            ['5', '6', '7', '8', '9', '1', '2', '3', '4'],
            ['8', '9', '1', '2', '3', '4', '5', '6', '7'],
            ['3', '4', '5', '6', '7', '8', '9', '1', '2'],
            ['6', '7', '8', '9', '1', '2', '3', '4', '5'],
            ['9', '1', '2', '3', '4', '5', '6', '7', '8']
        ]);
        const dataField = new DataField(table, logTable, data);
        dataField.initialize();

        this.dataField = dataField;
    }

    start() {
        // スタートボタンを押下したら呼び出される。
        // プレイヤーが問題を入力し終わっている前提。
        // 問題をログ登録する。
        // プレイヤーが問題を解き始める。データ変更をログ登録する。
        const dataField = this.dataField;
        dataField.start();
    }

    finish() {
        // チェックボタンを押下したら呼び出される。
        // 完成したかチェックする。
        // 完成した場合は完成メッセージを、してない場合は未完成メッセージを表示する。
        const dataField = this.dataField;
        dataField.finish();
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const table = document.getElementById('sudoku-table');
    const logTable = document.getElementById('log-table');

    const controller = new GameController(table, logTable);
    controller.initialize();

    document.getElementById('clear-button').addEventListener('click', () => {controller.initialize()});
    document.getElementById('start-button').addEventListener('click', () => {controller.start()});
    document.getElementById('check-button').addEventListener('click', () => {controller.finish()});
});
