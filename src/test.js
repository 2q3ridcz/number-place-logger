class FieldDataTest {
    run() {
        // 名前の最後に'Test'のつくメソッドを抽出し、すべて実行する。
        const testMethodNames = Object.getOwnPropertyNames(FieldDataTest.prototype).filter(v => v.endsWith('Test'));
        const results = testMethodNames.map(name => this[name]());
        console.log(`${this.constructor.name}: ${results.length} tests.`);
    }

    completedData() {
        return new FieldData([
            ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
            ['4', '5', '6', '7', '8', '9', '1', '2', '3'],
            ['7', '8', '9', '1', '2', '3', '4', '5', '6'],
            ['2', '3', '4', '5', '6', '7', '8', '9', '1'],
            ['5', '6', '7', '8', '9', '1', '2', '3', '4'],
            ['8', '9', '1', '2', '3', '4', '5', '6', '7'],
            ['3', '4', '5', '6', '7', '8', '9', '1', '2'],
            ['6', '7', '8', '9', '1', '2', '3', '4', '5'],
            ['9', '1', '2', '3', '4', '5', '6', '7', '8']
        ]);
    }

    constructor900Test() {
        const inputData = 1;
        try {
            new FieldData(inputData);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

    constructor901Test() {
        const inputData = `abc`;
        try {
            new FieldData(inputData);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

    constructor902Test() {
        const inputData = [`abc`];
        try {
            new FieldData(inputData);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

    constructor903Test() {
        const inputData = [['abc']];
        try {
            new FieldData(inputData);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

    constructor904Test() {
        const inputData = [['a'], ['a'], ['a'], ['a'], ['a'], ['a'], ['a'], ['a'], ['a']];
        try {
            new FieldData(inputData);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

    constructor905Test() {
        const inputData = [
            '123456789',
            '456789123',
            '789123456',
            '234567891',
            '567891234',
            '891234567',
            '345678912',
            '678912345',
            '912345678'
        ];
        try {
            new FieldData(inputData);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

     /**
     * Throws when int data is given.
     */ 
     constructor999ThrowsOnIntDataTest() {
        const inputData = [
            [1, 2, 3, 4, 5, 6, 7, 8, 9],
            [4, 5, 6, 7, 8, 9, 1, 2, 3],
            [7, 8, 9, 1, 2, 3, 4, 5, 6],
            [2, 3, 4, 5, 6, 7, 8, 9, 1],
            [5, 6, 7, 8, 9, 1, 2, 3, 4],
            [8, 9, 1, 2, 3, 4, 5, 6, 7],
            [3, 4, 5, 6, 7, 8, 9, 1, 2],
            [6, 7, 8, 9, 1, 2, 3, 4, 5],
            [9, 1, 2, 3, 4, 5, 6, 7, 8]
        ];
        try {
            new FieldData(inputData);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

     /**
     * Throws when disallowed data (like 'a') is given.
     */ 
     constructor999ThrowsOnDisAllowedLetterTest() {
        const inputData = [
            ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
            ['4', '5', '6', '7', '8', '9', '1', '2', '3'],
            ['7', '8', '9', '1', '2', '3', '4', '5', '6'],
            ['2', '3', '4', '5', '6', '7', '8', '9', '1'],
            ['5', '6', '7', '8', '9', '1', '2', '3', '4'],
            ['8', '9', '1', '2', '3', '4', '5', '6', '7'],
            ['3', '4', '5', '6', '7', '8', '9', '1', '2'],
            ['6', '7', '8', '9', '1', '2', '3', '4', '5'],
            ['9', '1', '2', '3', '4', '5', '6', '7', 'a']
        ];
        try {
            new FieldData(inputData);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

    isComplete001Test() {
        const data = new FieldData();
        const result = data.isComplete();
        const expect = false;
        console.assert(result === expect, {expect: expect, result: result});
    }

    isComplete002Test() {
        const data = this.completedData();
        const result = data.isComplete();
        const expect = true;
        console.assert(result === expect, {expect: expect, result: result});
    }

    getSquare001Test() {
        const data = this.completedData();
        const result = data.getSquare(0);

        const expect = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
        const values = result.data;
        const isEqual = values.length === expect.length && values.every((value, index) => value === expect[index]);
        console.assert(isEqual, {expect: expect, result: result});
    }

    getSquare002Test() {
        const data = this.completedData();
        const result = data.getSquare(5);

        const expect = ['8', '9', '1', '2', '3', '4', '5', '6', '7'];
        const values = result.data;
        const isEqual = values.length === expect.length && values.every((value, index) => value === expect[index]);
        console.assert(isEqual, {expect: expect, result: result});
    }

    getSquare901Test() {
        const data = this.completedData();
        try {
            data.getSquare(-1);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }

    getSquare902Test() {
        const data = this.completedData();
        try {
            data.getSquare(9);
        } catch(e) {
            console.assert(e.name === `TypeError`, e);
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    (new FieldDataTest).run();
});
